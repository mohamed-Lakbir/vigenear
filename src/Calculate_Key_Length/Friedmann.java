package Calculate_Key_Length;

import checkText.TextAnalysis;

import java.util.HashMap;

public class Friedmann{
    private TextAnalysis textAnalysis = new TextAnalysis();

    final private double iD = 0.076;
    final private double iZ = 0.0385;

    public Friedmann(){



    }

    public double friedmanTest(String text){

        double l= ((iD-iZ)*text.length())/((text.length()-1)* getIChiffre(text)-iZ*text.length()+iD);


        System.out.print(l);

        return l;

    }


    public double getIChiffre ( String text){
        double n =0;

        double n_Minus_1=0;

        HashMap<String,Integer> countLetters = textAnalysis.countBlocks(text,1);

        for (int value : countLetters.values()) {
            n = n+value;
            n_Minus_1 = n_Minus_1+value*(value-1);
        }


        double result = n_Minus_1/(n*(n-1));



        return  result;
    }


    
}
