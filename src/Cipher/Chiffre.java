package Cipher;

public interface Chiffre {
    void code(String key, String text);

    void decode(String key);
}
