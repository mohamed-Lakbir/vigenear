/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cipher;

/**
 *
 * @author sid93_000
 */
public class Vigenear implements Chiffre {
    String codeText="";
    String decodeText="";
    char   temp;
    int    temp2;

    public Vigenear(){}


    @Override
    public void code(String key, String text) {
        text=text.toUpperCase();
        key= key.toUpperCase();
        for (int i =0;i<text.length();i++){
            if(text.charAt(i)<65||text.charAt(i)>90){
                temp=text.charAt(i);
            }else{
                
                temp=(char)((text.charAt(i)-'A'+key.charAt(i%key.length())-'A')%26+'A');
            }   
            codeText=codeText+Character.toString(temp);    
        }  
    }

    @Override
    public void decode(String key) {
        key=key.toUpperCase();
        for (int i =0;i<codeText.length();i++){
            if(codeText.charAt(i)<65||codeText.charAt(i)>90){
                temp=codeText.charAt(i);
            }else{
                temp=(char)(codeText.charAt(i)-'A'-(key.charAt(i%key.length())-'A')%26+'A');
                temp2 = codeText.charAt(i)-'A'-(key.charAt(i%key.length())-'A');
                if(temp2<0){
                    temp=(char)(26+temp2+'A');
                }else{
                    temp=(char)(temp2+'A');
                }
            }
            
            decodeText=decodeText+Character.toString(temp);    
        }  
        
    }
    
    @Override
    public String toString(){
        return decodeText+
              "\n"+codeText;
    }

    public static void main (String[]args){


    }
    
    
}
