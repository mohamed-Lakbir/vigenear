package checkText;

import java.security.KeyStore;
import java.util.*;

public class TextAnalysis {

    private String temp;
    public TextAnalysis (){


    }


    public List<String> countBlocksKasisky(String text,int nGramLength){
        HashMap<String,Integer> unsortetMap = new HashMap <>();


        for ( int i =0;i<text.length()-nGramLength-1;i++){
            temp="";
            for(int j=1;j<=nGramLength;j++){
                temp=temp+text.charAt(i+j);
            }

            if(unsortetMap.containsKey(temp)){
                unsortetMap.put(temp,unsortetMap.get(temp)+1);
            }else {
                unsortetMap.put(temp,1);
            }

        }

        return sortHashmap(unsortetMap);



    }



    public HashMap<String, Integer> countBlocks( String text, int nGramLength){


        HashMap<String,Integer> unsortetMap = new HashMap <>();


        for ( int i =0;i<text.length()-nGramLength-1;i++){
            temp="";
            for(int j=1;j<=nGramLength;j++){
                temp=temp+text.charAt(i+j);
            }

            if(unsortetMap.containsKey(temp)){
                unsortetMap.put(temp,unsortetMap.get(temp)+1);
            }else {
                unsortetMap.put(temp,1);
            }

        }

        return unsortetMap;

    }



    public List<String> sortHashmap(HashMap<String,Integer>unsortetMap){


        List<Map.Entry <String,Integer>> list = new LinkedList <>(unsortetMap.entrySet());
        List<String> sortedList = new LinkedList <>();
        Collections.sort(list, new Comparator <Map.Entry <String, Integer>>() {
            @Override
            public int compare ( Map.Entry <String, Integer> o1, Map.Entry <String, Integer> o2 ) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });


        for(Map.Entry <String,Integer> item:list){
            ((LinkedList<String>) sortedList).addFirst(item.getKey());
        }

        return sortedList;

    }

    public List<String> sortHashmapKasisky(HashMap<String,Integer>unsortetMap){


        List<Map.Entry <String,Integer>> list = new LinkedList <>(unsortetMap.entrySet());
        List<String> sortedList = new LinkedList <>();
        Collections.sort(list, new Comparator <Map.Entry <String, Integer>>() {
            @Override
            public int compare ( Map.Entry <String, Integer> o1, Map.Entry <String, Integer> o2 ) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });


        for(Map.Entry <String,Integer> item:list){
            if(item.getValue()<3){
            ((LinkedList<String>) sortedList).addFirst(item.getKey());
            }
        }

        return sortedList;

    }





}
